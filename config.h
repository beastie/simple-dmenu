/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching*/

/* -fn option overrides fonts[0]; default X11 font or font set */
static char font[] = "mono:size=9:antialias=true:autohint=true";
static const char *fonts[] = {
	font,
	"NotoColorEmoji:pixelsize=14:antialias=true:autohint=true",
};

static char *prompt      = NULL;      /* -p  option; prompt to the left of input field */

static char normfgcolor[] = "#02131A";
static char normbgcolor[] = "#847d81";
static char selfgcolor[]  = "#02131A";
static char selbgcolor[]  = "#985966";
static char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { normfgcolor, normbgcolor },
	[SchemeSel]  = { selfgcolor,  selbgcolor  },
	[SchemeOut]  = { "#000000",   "#00ffff" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
	{ "font",        STRING, &font },
	{ "dmenu.color0", STRING, &normfgcolor },
	{ "dmenu.color8", STRING, &normbgcolor },
	{ "dmenu.color0",  STRING, &selfgcolor },
	{ "dmenu.color14",  STRING, &selbgcolor },
	{ "prompt",      STRING, &prompt },
};
